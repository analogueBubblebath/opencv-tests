package ml.bubblebath.controller

import javafx.scene.image.Image
import ml.bubblebath.view.MainView
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import tornadofx.Controller
import tornadofx.runLater
import java.nio.file.Paths

class MainController : Controller() {
    companion object {
        const val TEMP_FILE_NAME = "temp_stian.png"
    }
    val view: MainView by inject()

    var stian = Imgcodecs.imread("stian.jpg", Imgcodecs.IMREAD_COLOR)

    private fun applyTransformation(imgproc: (result: Mat) -> Unit) {
        val result = Mat()
        imgproc(result)
        Imgcodecs.imwrite(TEMP_FILE_NAME, result)
        updateImageview()
    }

    fun toGrayScale() {
        applyTransformation {
            Imgproc.cvtColor(stian, it, Imgproc.COLOR_BGR2GRAY)
        }
    }

    fun applyThreshold() {
        applyTransformation {
            Imgproc.threshold(stian, it, 0.0, 255.0, Imgproc.THRESH_BINARY)
        }
    }

    fun applyGauss() {
        applyTransformation {
            Imgproc.GaussianBlur(stian, it, Size(stian.width().toDouble(), stian.height().toDouble()), 2.0)
        }
    }

    fun applyBlur() {
        applyTransformation {
            Imgproc.blur(stian, it, Size(stian.width().toDouble(), stian.height().toDouble()))
        }
    }

    fun applyCanny() {
        applyTransformation {
            Imgproc.Canny(stian, it, stian.width().toDouble(), stian.height().toDouble())
        }
    }

    fun applySobel() {
        applyTransformation {
            Imgproc.Sobel(stian, it, CvType.CV_64F, 1, 0, 3)
        }
    }

    fun applyLaplacian() {
        applyTransformation {
            Imgproc.Laplacian(stian, it, CvType.CV_64F, 1, 3.0, 3.0)
        }
    }

    fun updateImageview() {
        runLater {
            view.imageviewResult.image = Image(Paths.get("temp_stian.png").toFile().inputStream())
        }
    }

    fun flushImageView() {
        runLater {
            view.imageviewResult.image = Image("stian.jpg")
        }
    }
}