package ml.bubblebath.app

import javafx.stage.Stage
import ml.bubblebath.view.MainView
import org.opencv.core.Core
import tornadofx.App
import java.nio.file.Paths

class Main : App(MainView::class) {
    override fun start(stage: Stage) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
        println(System.getProperty("java.library.path"))
        super.start(stage)
    }

    override fun stop() {
        Paths.get("temp_stian.png").toFile().delete()
        super.stop()
    }
}