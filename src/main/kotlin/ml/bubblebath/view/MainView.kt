package ml.bubblebath.view

import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.ToggleGroup
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import ml.bubblebath.controller.MainController
import tornadofx.*

class MainView : View("OpenCV") {
    val controller: MainController by inject()

    var imageviewResult: ImageView by singleAssign()

    override val root = anchorpane {
        hbox(spacing = 16.0) {
            anchorpaneConstraints {
                leftAnchor = 16.0
                rightAnchor = 16.0
                topAnchor = 16.0
                bottomAnchor = 16.0
            }

            alignment = Pos.CENTER

            vbox(spacing = 16.0) {
                label("Исходное изображение")
                imageview {
                    image = Image("stian.jpg")
                }

                val toggleGroup = ToggleGroup()

                radiobutton("Без фильтров", toggleGroup) {
                    action {
                        controller.flushImageView()
                    }
                }

                radiobutton("В оттенки серого", toggleGroup) {
                    action {
                        controller.toGrayScale()
                    }
                }

                radiobutton("Бинаризация", toggleGroup) {
                    action {
                        controller.applyThreshold()
                    }
                }

                radiobutton("Фильтр Гаусса", toggleGroup) {
                    action {
                        controller.applyGauss()
                    }
                }

                radiobutton("Усреднение", toggleGroup) {
                    action {
                        controller.applyBlur()
                    }
                }

                radiobutton("Фильтр Канни", toggleGroup) {
                    action {
                        controller.applyCanny()
                    }
                }

                radiobutton("Фильтр Собеля", toggleGroup) {
                    action {
                        controller.applySobel()
                    }
                }

                radiobutton("Фильтр Лапласса", toggleGroup) {
                    action {
                        controller.applyLaplacian()
                    }
                }
            }

            separator(Orientation.VERTICAL)

            vbox(spacing = 16.0) {
                label("Изображение с фильтром")
                imageviewResult = imageview {
                    image = Image("stian.jpg")
                }
            }
        }
    }
}